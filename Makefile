PREFIX = /usr/local
LOCALE = /usr/share/locale-langpack

install:
	@echo installing executable file to ${PREFIX}/bin
	@mkdir -p ${PREFIX}/bin
	@cp -f pyrecipes ${PREFIX}/bin
	@chmod 755 ${PREFIX}/bin/pyrecipes
	@echo installing language packs to ${LOCALE}
	@msgfmt --output-file=${LOCALE}/fr/LC_MESSAGES/pyrecipes.mo messages_fr.po
	@msgfmt --output-file=${LOCALE}/en/LC_MESSAGES/pyrecipes.mo messages_en.po
	@echo "if required, copy css dir to your export dir"

uninstall:
	@echo removing executable file from ${PREFIX}/bin
	@rm ${PREFIX}/bin/pyrecipes
