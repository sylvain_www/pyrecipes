Yet another cooking recipe manager
==================================

The program is designed to facilitate the collection and printing of savoury recipes, for the creation of a hardcover cook book or recipe cards!

##Interface

![screenshot](http://bytebucket.org/sylvain_www/pyrecipes/raw/b0e77b24d96a70f8d1ffc74997fcc7818634436b/screenshot.png)

##Export of a recipe (HTML, themed with CSS)
![screenshot](http://bytebucket.org/sylvain_www/pyrecipes/raw/8fbad794a53f93feb1e931391018e4554ccf2bbb/screenshot2.png)

The program is console oriented, uses curses for the frontend and sqlite for the database.

I wrote this code to teach myself Python. Please have mercy!
